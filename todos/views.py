from django.shortcuts import render
from todos.models import TodoList, TodoItem


# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todo_lists/list.html", context)

def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    tasks = todo_list.task_set.all()
    context = {
        'todo_list':todo_list,
        'tasks':tasks,
    }
    return render(request, 'todo_list_detail.html', context)
